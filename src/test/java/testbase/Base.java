package testbase;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;

public class Base {

	public static WebDriver driver;
	public static Properties prop;
	public static WebDriverWait wait;
	public static ExtentHtmlReporter reporter;
	public static ExtentReports reports;
	public static ExtentTest test;
	static String fileName = new SimpleDateFormat("yyyy-MM-dd'.html'").format(new Date());
	static
	{
		reporter = new ExtentHtmlReporter("C:\\Users\\rrabade\\roshan\\Framework\\Reports\\"+fileName);
		reports = new ExtentReports();
		reports.attachReporter(reporter);
		reports.setSystemInfo("OS", "Windows 10");
		reports.setSystemInfo("Environment", "QA");
		reporter.config().setChartVisibilityOnOpen(true);
		reporter.config().setDocumentTitle("Execution Reports");
		reporter.config().setReportName("OAM UK Automation testing");
		reporter.config().setTestViewChartLocation(ChartLocation.TOP);
		reporter.config().setTheme(Theme.STANDARD);
		reporter.config().setTimeStampFormat("EEEE, MMMM dd, yyyy, hh:mm a '('zzz')'");
	}
	
	public Base()
	{
		try {
			prop = new Properties();
			FileInputStream fis = new FileInputStream("C:\\Users\\rrabade\\roshan\\Framework\\Ikano_NewLook\\src\\test\\java\\configuration\\config.properties");
			prop.load(fis);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void load(String browser)
	{
		if(browser.equalsIgnoreCase("chrome"))
		{
			System.setProperty("webdriver.chrome.driver", "C:\\Users\\rrabade\\roshan\\Framework\\Software\\chromedriver.exe");
			driver = new ChromeDriver();
			reports.setSystemInfo("Browser", "Chrome");
		}
		else if(browser.equalsIgnoreCase("edge"))
		{
			System.setProperty("webdriver.edge.driver", "C:\\Users\\rrabade\\roshan\\Framework\\Software\\MicrosoftWebDriver.exe");
			driver = new EdgeDriver();
			reports.setSystemInfo("Browser", "Edge");
		}
		else if(browser.equalsIgnoreCase("firefox"))
		{
			System.setProperty("webdriver.gecko.driver", "C:\\Users\\rrabade\\roshan\\Framework\\Software\\geckodriver.exe");
			driver = new FirefoxDriver();
			reports.setSystemInfo("Browser", "FireFox");
		}
				
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		driver.get(prop.getProperty("url"));
	}
	
	public String checkBrowser()
	{
		 Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
		 String browserName = caps.getBrowserName().toUpperCase();
		 return browserName;
	}
}
