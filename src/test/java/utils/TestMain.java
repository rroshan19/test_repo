package utils;

import java.util.List;

import org.testng.TestNG;
import org.testng.collections.Lists;

import testbase.Base;;

public class TestMain extends Base {

	public static void main(String[] args) {
		
		TestNG testSuite = new TestNG();
		
		List<String> suites = Lists.newArrayList();
		suites.add("C:\\Users\\rrabade\\roshan\\Framework\\Ikano_NewLook\\testng.xml");//path to xml..
		testSuite.setTestSuites(suites);
		testSuite.run();
	}

}
