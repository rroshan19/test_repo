package utils;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.io.FileHandler;

public class Screenshots {

	public static String getscreenshots(WebDriver driver, String screenshotName)
	{
		//String dateName = new SimpleDateFormat("MM-dd-hh-mm:ss").format(new Date());
		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		String path= "C:\\Users\\rrabade\\roshan\\Framework\\Screenshots\\Fail\\" + screenshotName + ".png";
		try {
			FileHandler.copy(scrFile, new File(path));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return path;
	}
}
