package utils;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.io.FileHandler;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.aventstack.extentreports.Status;

import testbase.Base;

public class Listener extends Base implements ITestListener {

	public void onTestStart(ITestResult result) {
		// TODO Auto-generated method stub
		
	}

	public void onTestSuccess(ITestResult result) {
		
		String filename = result.getName();
		//String date = new SimpleDateFormat("MM-dd-hh-mm:ss").format(new Date());
		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		try {
			FileHandler.copy(scrFile, new File("C:\\Users\\rrabade\\roshan\\Framework\\Screenshots\\Pass\\" + filename +  ".png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	public void onTestFailure(ITestResult result) {
		
		String filename = result.getName();
		test.log(Status.FAIL, filename + " got failed");
		//String date = new SimpleDateFormat("MM-dd-hh-mm:ss").format(new Date());
		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		try {
			FileHandler.copy(scrFile, new File("C:\\Users\\rrabade\\roshan\\Framework\\Screenshots\\Fail\\" + filename + ".png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void onTestSkipped(ITestResult result) {
		// TODO Auto-generated method stub
		
	}

	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		// TODO Auto-generated method stub
		
	}

	public void onStart(ITestContext context) {
		// TODO Auto-generated method stub
		
	}

	public void onFinish(ITestContext context) {
		// TODO Auto-generated method stub
		
	}

}
