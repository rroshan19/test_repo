package pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.Status;

import testbase.Base;

public class LoginPage extends Base{

	@FindBy(id="username")
	private WebElement username;
	
	@FindBy(name="password")
	private WebElement pwd;
	
	@FindBy(xpath="//input[@type='submit']")
	private WebElement submit;
	
	@FindBy(xpath="//div[@class='bp-g-include']/div/img[@title='logo_ikano@2x.png']")
	private WebElement ikanoLogo;
	
	@FindBy(xpath="//div[@class='bp-g-include']/div/img[@title='logo_ikano_newlook_oam@2x.png']")
	private WebElement newLookLogo;
	
	@FindBy(xpath="//div[@class='bp-g-include']/div/img[@title='IKEA_Trademark_BY.png']")
	private WebElement ikeaLogo;
	
	@FindBy(xpath="//label[contains(text(), 'be')]")
	private WebElement chkbox;
	
	@FindBy(xpath="//div[@class='ikano-notification-wrapper ng-scope bg-error popup']")
	List<WebElement> emailIdError;
	
	@FindBy(xpath="//div[contains(text(), 'Please enter your valid password')]")
	private WebElement pwdError;
		
	public LoginPage()
	{
		PageFactory.initElements(driver, this);
	}
	
	public String verifyLoginPageTitle()
	{
		return driver.getTitle();
	}
	
	public boolean verifyIkanoLogo()
	{
		return ikanoLogo.isDisplayed();
	}
	
	public boolean verifyNewLookLogo()
	{
		return newLookLogo.isDisplayed();
	}
	
	public boolean verifyIkeaLogo()
	{
		return ikeaLogo.isDisplayed();
	}
	
	public void verifycheckbox() throws InterruptedException
	{
		Thread.sleep(10000);
		
		if(!chkbox.isSelected())
		{
			test.log(Status.INFO, "Checkbox is toggled off");
		}
		else
		{
			test.log(Status.INFO, "Checkbox is toggled On");
			chkbox.click();
		}
	}
	
	public String getAlertMsg()
	{
		String msg = null;
		for(WebElement e : emailIdError) {
				msg= e.getText();
			}
		return msg;		
	}
	
	
	public HintPage userlogin(String ID, String password) throws InterruptedException
	{
			Thread.sleep(5000);
			username.clear();
			username.sendKeys(ID);
			test.log(Status.PASS, "Entered Username");
			Thread.sleep(2000);
			pwd.clear();
			pwd.sendKeys(password);
			test.log(Status.PASS, "Entered Password");
			submit.click();
			test.log(Status.PASS, "Clicked on Submit Button");
		
			if(emailIdError.size()!=0)
			{
				test.log(Status.INFO, "Error msg is :- " + getAlertMsg());
				test.log(Status.PASS, "Credential are Incorrect");
				driver.navigate().refresh();
			}
			else
			{
				test.log(Status.INFO, "Credentials are correct");
			}
		
		return new HintPage();
	}
}
