package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

import testbase.Base;

public class HomePage extends Base{

	@FindBy(xpath="//div[@class='bp-g-include']/div/img[@title='logo_ikano@2x.png']")
	private WebElement ikanoLogo;
	
	@FindBy(xpath="//div[@class='bp-g-include']/div/img[@title='logo_ikano_newlook_oam@2x.png']")
	private WebElement newLookLogo;
	
	@FindBy(xpath="//div[@class='bp-g-include']/div/img[@title='IKEA_Trademark_BY.png']")
	private WebElement ikeaLogo;
	
	@FindBy(xpath="//span[@class='center-block ng-scope']/strong")
	private WebElement profileName;
	
	@FindBy(xpath="//a[@title='Logout']")
	private WebElement logout;
	
	@FindBy(xpath="//li/a[@class=contains(text(), 'HOME')]")
	private WebElement home;
	
	@FindBy(xpath="//li/a[@class=contains(text(), 'MY CARD')]")
	private WebElement mycard;
	
	@FindBy(xpath="//li/a[@class=contains(text(), 'STATEMENTS')]")
	private WebElement stmt;
	
	@FindBy(xpath="//li/a[@class=contains(text(), 'MAKE A PAYMENT')]")
	private WebElement payment;
	
	@FindBy(xpath="//li/a[@class=contains(text(), 'HELP CENTRE')]")
	private WebElement help;
	
	@FindBy(xpath="//li/a[@class=contains(text(), 'MY PROFILE')]")
	private WebElement profile;
	
	@FindBy(xpath="//div[@class='personal-details ng-scope']/form/div[1]/div[2]/div[2]/i[@role='button']")
	private WebElement editEmail;
	
	@FindBy(xpath="//input[@name='emialID']")
	private WebElement emailID;
	
	@FindBy(xpath="//button[@class='save-btn']")
	private WebElement savebtn;
	
	@FindBy(xpath="//button[contains(text(), 'Cancel')]")
	private WebElement cancelbtn;
	
	@FindBy(xpath="//button[contains(text(), 'Update')]")
	private WebElement updatebtn;
	
	@FindBy(xpath="//a[contains(text(), 'SECURITY SETTINGS')]")
	private WebElement securitybtn;
	
	@FindBy(xpath="//div[@id='accordion']/div[1]")
	private WebElement firstdropdown;
	
	@FindBy(xpath="//div[@id='accordion']/div[2]")
	private WebElement secunddropdown;
	
	@FindBy(xpath="//div[@id='accordion']/div[3]")
	private WebElement thirddropdown;
		
	public HomePage()
	{
		PageFactory.initElements(driver, this);
	}
	
	public void verifyProfileName()
	{
		Assert.assertEquals(profileName.getText(), prop.getProperty("ProfileName"));
	}
	
	public void clickOnHome() throws InterruptedException
	{
		Thread.sleep(3000);
		home.click();
	}
	
	public void clickOnMyCard() throws InterruptedException
	{
		Thread.sleep(3000);
		mycard.click();
	}
	
	public void clickOnStmt() throws InterruptedException
	{
		Thread.sleep(3000);
		stmt.click();
	}
	
	public void clickOnPayment() throws InterruptedException
	{
		Thread.sleep(3000);
		payment.click();
	}
	
	public void clickOnHelp() throws InterruptedException
	{
		Thread.sleep(3000);
		help.click();
	}
	
	public void clickOnProfile() throws InterruptedException
	{
		Thread.sleep(3000);
		profile.click();
	}
	
	public void editprofileEmailID() throws InterruptedException
	{
		Thread.sleep(3000);
		editEmail.click();
		test.log(Status.INFO, "Clicked on Edit EmailID button");
		
		Thread.sleep(3000);
		emailID.clear();
		test.log(Status.INFO, "Cleared EmailID texyBox");
		
		Thread.sleep(3000);
		emailID.sendKeys("roshan.rabade@capgemini.co");
		test.log(Status.PASS, "Entered new EmailID in textbox");

		Thread.sleep(3000);
		savebtn.click();
		test.log(Status.INFO, "Clicked on Save button");
	
		Thread.sleep(3000);
		boolean status_cancelbtn = cancelbtn.isDisplayed();
		Assert.assertEquals(true, status_cancelbtn);
		test.log(Status.PASS, "Cancel button is visible");
		
		Thread.sleep(3000);
		boolean status_update = updatebtn.isDisplayed();
		Assert.assertEquals(true, status_update);
		test.log(Status.PASS, "Update button is visible");

		Thread.sleep(3000);
		cancelbtn.click();
		test.log(Status.INFO, "clicked on cancel button");
	}
	
	public void clickOnSecuritytab() throws InterruptedException
	{
		securitybtn.click();
		test.log(Status.INFO, "Clicked on Security tab");
		
		Thread.sleep(2000);
		firstdropdown.click();
		test.log(Status.INFO, "clicked on first dropdown");
		
		Thread.sleep(2000);
		secunddropdown.click();
		test.log(Status.INFO, "clicked on second dropdown");
		
		Thread.sleep(2000);
		thirddropdown.click();
		test.log(Status.INFO, "clicked on third dropdown");
		
	}
	
	public LoginPage logout()
	{
		logout.click();
		return new LoginPage();		
	}
	
	public void closeBrowser()
	{
		driver.quit();
	}
}
