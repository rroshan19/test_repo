package pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import testbase.Base;

public class HintPage extends Base {

	@FindBy(id="mword")
	WebElement word;
	
	@FindBy(xpath="//input[@type='submit']")
	WebElement submit;
	
	@FindBy(xpath="//strong[@class='highlight ng-binding'] ")
	WebElement email;
	
	public HintPage()
	{
		PageFactory.initElements(driver, this);
	}
	
	public void verifyEmail()
	{
		System.out.println(email.getText());
		Assert.assertEquals(email.getText(), prop.getProperty("userID"));
	}
	
	public HomePage setMemorableWord() throws InterruptedException
	{
		word.sendKeys(prop.getProperty("hint"));
		submit.click();
		
		//JavascriptExecutor executor = (JavascriptExecutor)driver;
		//executor.executeScript("arguments[0].click();", submit);
		return new HomePage();
	}
}
