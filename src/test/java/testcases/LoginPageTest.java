package testcases;

import java.io.IOException;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;

import pages.HintPage;
import pages.HomePage;
import pages.LoginPage;
import testbase.Base;
import utils.Screenshots;

@Listeners(utils.Listener.class)

public class LoginPageTest extends Base {

	LoginPage login;
	HintPage hint;
	HomePage home;

	public LoginPageTest() {
		super();
	}

	@BeforeClass
	@Parameters("browser")
	public void start(String browser) {
		load(browser);
		login = new LoginPage();
	}

	@Test(priority = 1, description = "verification of Logo")
	public void verifyLogoTest() throws IOException {
		test = reports.createTest(checkBrowser() + ":- LoginSenarioTest", "Validating Logo");
		boolean ikanologo = login.verifyIkanoLogo();
		Assert.assertTrue(ikanologo);
		test.log(Status.PASS, "Ikano logo is visible");

		boolean ikealogo = login.verifyIkeaLogo();
		Assert.assertTrue(ikealogo);
		test.log(Status.PASS, "IKEA logo is visible");
	}

	@Test(priority = 2, description = "verfication of checkbox")
	public void verifychkboxTest() throws InterruptedException {
		test = reports.createTest(checkBrowser() + ":- LoginSenarioTest", "Validating Checkbox");
		login.verifycheckbox();
	}

	@Test(dataProvider = "getData", priority = 3, description = "Login verification")
	public void credentialTest(String username, String password) throws InterruptedException {
		test = reports.createTest(checkBrowser() + ":- LoginSenarioTest", "Validating login credential");
		hint = login.userlogin(username, password);
	}

	@Test(priority = 4, description = "Logout")
	public void logintest() throws InterruptedException {
		test = reports.createTest(checkBrowser() + ":- LoginSenarioTest", "Hint Page test");
		test.log(Status.PASS, "Logged in successfully");
		Thread.sleep(2000);
		hint.verifyEmail();
		test.log(Status.PASS, "Verified email");
		home = hint.setMemorableWord();
		test.log(Status.PASS, "Entered memorable word & Navigated to Home Page");
		home.verifyProfileName();
		test.log(Status.INFO, "Verified Profile Name");
	}

	@Test(priority = 5, description = "Logout")
	public void logoutTest() {
		test = reports.createTest(checkBrowser() + ":- LoginSenarioTest", "Logout Test");
		home.logout();
		test.log(Status.PASS, "Successfully navigated to loginPage");
	}

	@AfterMethod
	public void addScreenshotToExtentReport(ITestResult result) throws IOException {
		if (result.getStatus() == ITestResult.SUCCESS) {
			test.log(Status.PASS, MarkupHelper.createLabel(result.getName() + "Test case passed", ExtentColor.GREEN));
		} else if (result.getStatus() == ITestResult.FAILURE) {
			String screenshotpath = Screenshots.getscreenshots(driver, result.getName());
			test.log(Status.FAIL,
					MarkupHelper.createLabel(result.getName() + "Test case got failed :- ", ExtentColor.RED));
			test.fail(result.getThrowable());
			test.fail("Screenshot :- " + test.addScreenCaptureFromPath(screenshotpath));
		}
		reports.flush();
	}

	@AfterClass
	public void stop() {
		reports.flush();
		driver.close();
	}

	@DataProvider
	public Object[][] getData() {
		Object[][] data = new Object[2][2];

		data[0][0] = "testuser@capgemini.com";
		data[0][1] = "Ikano@1234";

		data[1][0] = "amoolyaikea@uat.com";
		data[1][1] = "Test@123";

		return data;
	}
}
