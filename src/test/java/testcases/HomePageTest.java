package testcases;

import java.io.IOException;

import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;

import pages.HintPage;
import pages.HomePage;
import pages.LoginPage;
import testbase.Base;
import utils.Screenshots;

@Listeners(utils.Listener.class)

public class HomePageTest extends Base{

	LoginPage login;
	HintPage hint;
	HomePage home;
	
	public HomePageTest()
	{
		super();
	}
	
	@BeforeClass
	@Parameters("browser")
	public void start(String browser) throws InterruptedException
	{
		load(browser);
		login = new LoginPage();
	}
	
	@Test(priority=1)
	public void TC_001_loginCredentialTest() throws InterruptedException
	{
		test = reports.createTest(checkBrowser() + ":- LoginPageTest", "Validating Credential");
		hint = login.userlogin(prop.getProperty("userID"), prop.getProperty("Password"));
		test.log(Status.PASS, "Logged in successfully");
		home = hint.setMemorableWord();
		test.log(Status.PASS, "Entered memorable word & Navigated to Home Page");
		Thread.sleep(1000);
	}
	
	@Test(priority=2)
	public void TC_002_homePageTest() throws InterruptedException
	{
		test = reports.createTest(checkBrowser() + ":- HomePageTest", "Click on Each Page");
		Thread.sleep(2000);
		home.clickOnMyCard();
		test.log(Status.INFO, "clicked on MyCard section");
		home.clickOnStmt();
		test.log(Status.INFO, "clicked on Statement section");
		home.clickOnPayment();
		test.log(Status.INFO, "clicked on Payment section");
		home.clickOnHelp();
		test.log(Status.INFO, "clicked on Help section");
		home.clickOnProfile();
		test.log(Status.INFO, "clicked on Profile section");
	}
	
	@Test(priority=3)
	public void TC_003_profileSectionTest() throws InterruptedException
	{
		test = reports.createTest(checkBrowser() + ":- ProfileSectionTest", "Try to modify EmailID");
		home.editprofileEmailID();
		test.log(Status.PASS, "Successfully tried to modify emaidID");
	}
	
	@Test(priority=4)
	public void TC_004_securitytabTest() throws InterruptedException
	{
		test = reports.createTest(checkBrowser() + ":- SecuritytabTest", "Try to click on each drop down");
		home.clickOnSecuritytab();
		test.log(Status.PASS, "Successfully clicked on all drop down");
		home.clickOnHome();
		Thread.sleep(1000);
	}
	
	@Test(priority=5)
	public void TC_005_logoutTest() throws InterruptedException
	{
		test = reports.createTest(checkBrowser() + ":- LogoutTest", "Navigate to Login Page");
		home.logout();
		test.log(Status.PASS, "Successfully navigated to loginPage");
		reports.flush();
		Thread.sleep(2000);
		
	}
	
	@AfterMethod
	public void addScreenshotToExtentReport(ITestResult result) throws IOException
	{
		if(result.getStatus()==ITestResult.SUCCESS)
		{
            test.log(Status.PASS, MarkupHelper.createLabel(result.getName()+" Test case passed", ExtentColor.GREEN));
		}
		else if(result.getStatus()==ITestResult.FAILURE)
		{
			String screenshotpath = Screenshots.getscreenshots(driver, result.getName());
            test.log(Status.FAIL, MarkupHelper.createLabel(result.getName()+" Test case got failed :- ", ExtentColor.RED));
            test.fail(result.getThrowable());            
			test.fail("Screenshot :- " + test.addScreenCaptureFromPath(screenshotpath));
		}
		reports.flush();
	}
	
	@AfterClass
	public void quitBrowser () {
		home.closeBrowser();
	}
	
}
